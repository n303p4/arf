#!/usr/bin/env python3

"""
Go through one or more directories, find all files that match at least a certain image resolution and/or ratio, and
move them to another directory.
"""

import argparse
import logging
import os
import shutil
import sys
from typing import Iterable, Tuple

from PIL import Image


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
_handler = logging.StreamHandler()
_handler.setFormatter(logging.Formatter("%(asctime)-15s %(levelname)-8s : %(message)s"))
_handler.setLevel(logging.INFO)
logger.addHandler(_handler)

class ConflictResolutionType:
    ASK = ["a", "ask"]
    RENAME = ["r", "rename"]
    OVERWRITE = ["o", "overwrite"]
    SKIP = ["s", "skip"]
CONFLICT_RESOLUTION_TYPES = (
    ConflictResolutionType.ASK + ConflictResolutionType.OVERWRITE + \
    ConflictResolutionType.RENAME + ConflictResolutionType.SKIP
)
CONFLICT_RESOLUTION_TYPES_NO_ASK = (
    ConflictResolutionType.OVERWRITE + ConflictResolutionType.RENAME + ConflictResolutionType.SKIP
)


def conflict_resolution_type(type_:str):
    """Check if a string is a valid conflict resolution type."""
    type_ = type_.lower()
    if type_ not in CONFLICT_RESOLUTION_TYPES:
        raise ValueError("Not a valid conflict resolution type.")
    return type_


def aspect_ratio(aspect_ratio_:str):
    """Convert an aspect ratio represented as a string W:H into a float."""
    try:
        return float(aspect_ratio_)
    except Exception:
        pass
    if aspect_ratio_.count(":") != 1:
        raise ValueError("Not a valid aspect ratio.")
    aspect_ratio_ = aspect_ratio_.split(":")
    aspect_ratio_ = int(aspect_ratio_[0])/int(aspect_ratio_[1])
    return aspect_ratio_


def existing_directory(directory:str):
    """Test a string to see if it is an existing directory."""
    directory = os.path.abspath(directory)
    if os.path.isdir(directory):
        return directory
    raise ValueError("Not an existing directory.")


def matches_aspect_ratio(image:Image.Image, test_ratio:float, tolerance:float=0.05):
    """Check if an image returned by PIL matches a certain aspect ratio."""
    image_ratio = image.width/image.height
    if abs(image_ratio - test_ratio) > tolerance:
        return False
    return True


def find_images(srcdirs:Iterable[str], dstdir:str, min_width:int, min_height:int, aspect_ratios:Iterable[float],
                excdirs:Iterable[str], recursive:bool=False, copy:bool=False, link:bool=False, 
                conflict_resolution_type_=ConflictResolutionType.ASK[0], include_hidden=False,
                verbose=False):
    """Main function of this library/utility."""
    excdirs = excdirs or []
    if excdirs and not recursive:
        logger.error("Cannot exclude a directory without recursive.")
    for srcdir in srcdirs:
        if srcdir in excdirs:
            logger.error("Cannot exclude a source directory.")
            sys.exit(1)
    aspect_ratios = aspect_ratios or []
    if not any((min_width, min_height, aspect_ratios)):
        logger.error("An aspect ratio, width, or height is required!")
        sys.exit(1)
    if copy and link:
        logger.error("Cannot both copy and link files at once.")
        sys.exit(1)
    if copy:
        action = shutil.copy2
        action_word_present = "copy"
        action_word_past = "copied"
    elif link:
        action = lambda src, dst: os.symlink(src, dst, target_is_directory=False)
        action_word_present = "link"
        action_word_past = "linked"
    else:
        action = shutil.move
        action_word_present = "move"
        action_word_past = "moved"
    if os.path.isfile(dstdir):
        logger.error(f"\"{dstdir}\" is an existing file! Cannot continue.")
        sys.exit(1)
    elif not os.path.isdir(dstdir):
        try:
            os.makedirs(dstdir)
        except PermissionError:
            logger.error(f"Could not create directory \"{dstdir}\": Permission denied.")
            sys.exit(1)
        logger.info(f"Created directory \"{dstdir}\".")
    files_acted = 0
    files_unacted = 0
    files_unacted_hidden = 0
    files_unable_to_act = 0
    for srcdir in srcdirs:
        for dirpath, _, filenames in os.walk(srcdir):
            if not recursive and dirpath != srcdir:
                continue
            if any(map(dirpath.startswith, excdirs)):
                logger.info(f"Skipping \"{dirpath}\" because it was manually excluded.")
                continue
            logger.info(f"Scanning \"{dirpath}\"...")
            for filename in filenames:
                fullpath_source = os.path.abspath(os.path.join(dirpath, filename))

                if any(node.startswith(".") for node in fullpath_source.split(os.sep)) and not include_hidden:
                    if verbose:
                        logger.info(f"Skipping hidden file/directory \"{fullpath_source}\".")
                    files_unacted_hidden += 1
                    continue

                fullpath_destination = os.path.abspath(os.path.join(dstdir, filename))

                if fullpath_source == fullpath_destination:
                    files_unacted += 1
                    continue

                try:
                    image = Image.open(fullpath_source)
                except Exception:
                    if verbose:
                        logger.info(f"Could not read \"{fullpath_source}\" as an image.")
                    files_unacted += 1
                    continue

                if min_width and image.width < min_width:
                    if verbose:
                        logger.info(f"\"{fullpath_source}\" does not meet minimum width of {min_width}px.")
                    files_unacted += 1
                    continue

                if min_height and image.height < min_height:
                    if verbose:
                        logger.info(f"\"{fullpath_source}\" does not meet minimum height of {min_height}px.")
                    files_unacted += 1
                    continue

                if aspect_ratios and not any(matches_aspect_ratio(image, ar) for ar in aspect_ratios):
                    if verbose:
                        logger.info((f"\"{fullpath_source}\" does not meet aspect ratios "
                                     f"{', '.join(str(round(ar, 3)) for ar in aspect_ratios)}."))
                    files_unacted += 1
                    continue

                this_conflict_resolution_type_ = conflict_resolution_type_

                if os.path.lexists(fullpath_destination):
                    if this_conflict_resolution_type_ in ConflictResolutionType.ASK:
                        while this_conflict_resolution_type_ not in CONFLICT_RESOLUTION_TYPES_NO_ASK:
                            this_conflict_resolution_type_ = input((f"\"{fullpath_destination}\" already exists! "
                                                                    "[r]ename/[o]verwrite/[s]kip? ")).lower()
                    if this_conflict_resolution_type_ in ConflictResolutionType.OVERWRITE:
                        try:
                            if os.path.isfile(fullpath_destination) or os.path.islink(fullpath_destination):
                                os.remove(fullpath_destination)
                            else:
                                os.rmdir(fullpath_destination)                            
                        except PermissionError:
                            logger.warning(f"Could not delete \"{fullpath_destination}\": Permission denied.")
                            files_unable_to_act += 1
                            continue
                        logger.info(f"Deleted \"{fullpath_destination}\".")
                    elif this_conflict_resolution_type_ in ConflictResolutionType.RENAME:
                        counter = 0
                        while os.path.lexists(fullpath_destination):
                            counter += 1
                            fullpath_destination = os.path.abspath(os.path.join(
                                dstdir, f"-{counter}.".join(filename.rsplit("."))
                            ))
                        logger.info(f"Will use \"{fullpath_destination}\" as destination.")
                    elif this_conflict_resolution_type_ in ConflictResolutionType.SKIP:
                        logger.info(f"Did not {action_word_present} \"{fullpath_source}\".")
                        files_unacted += 1
                        continue

                try:
                    action(fullpath_source, fullpath_destination)
                except PermissionError:
                    logger.warning((f"Could not {action_word_present} \"{fullpath_source}\" to "
                                    f"\"{fullpath_destination}\": Permission denied."))
                    files_unable_to_act += 1
                    continue
                files_acted += 1
                logger.info(f"{action_word_past.capitalize()} \"{fullpath_source}\" to \"{fullpath_destination}\".")

    results_string = f"{action_word_past.capitalize()} {files_acted} files to {dstdir}. {files_unacted} files ignored."
    if files_unacted_hidden:
        results_string += f" {files_unacted_hidden} hidden files ignored."
    if files_unable_to_act:
        results_string += f" Could not {action_word_present} {files_unable_to_act} files due to permission errors."
    logger.info(results_string)


def driver():
    """Command line driver."""
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("-i", "--source", metavar="SRCDIR", type=existing_directory, action="append", required=True,
                        help="Input directory to search through for images. You must specify at least one.")
    parser.add_argument("-o", "--destination", metavar="DSTDIR", type=os.path.abspath, required=True,
                        help="Output directory to move all images to. Required. Will be created if it doesn't exist.")
    parser.add_argument("-a", "--aspect-ratio", metavar="WIDTH:HEIGHT", type=aspect_ratio, action="append",
                        help=("Aspect ratio that an image must meet, either in the form WIDTH:HEIGHT or as a "
                              "decimal number. You may specify more than one. "
                              "A 5%% error is allowed in either direction. Either this, -x, or -y is required."))
    parser.add_argument("-x", "--min-width", metavar="WIDTH", type=int,
                        help="Minimum image width required, in pixels. Either this, -a, or -y is required.")
    parser.add_argument("-y", "--min-height", metavar="HEIGHT", type=int,
                        help="Minimum image height required, in pixels. Either this -a, or -x is required.")
    parser.add_argument("-c", "--copy", action="store_true",
                        help="Copy images instead of moving them. Cannot use with -l.")
    parser.add_argument("-l", "--symlink", action="store_true",
                        help=("Create links in the output directory instead of moving/copying images. "
                              "Cannot use with -c."))
    parser.add_argument("-r", "-R", "--recursive", action="store_true",
                        help="Recursively walk through subdirectories.")
    parser.add_argument("-e", "--exclude", metavar="EXCDIR", type=existing_directory, action="append",
                        help="Exclude a directory. Can only be used with recursive.")
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="Verbose output.")
    parser.add_argument("--res", "--resolve-conflicts", metavar="CONFLICTRES", type=conflict_resolution_type,
                        default=ConflictResolutionType.ASK[0],
                        help=("Conflict resolution. Must be one of "
                              "[a]sk, [r]ename, [o]verwrite, or [s]kip. Default is [a]sk."))
    parser.add_argument("--all", action="store_true",
                        help="Include files and directories that start with \".\". Otherwise, will ignore them.")
    args = parser.parse_args()
    find_images(args.source, args.destination, aspect_ratios=args.aspect_ratio, min_width=args.min_width,
                min_height=args.min_height, recursive=args.recursive, excdirs=args.exclude, copy=args.copy,
                link=args.symlink, conflict_resolution_type_=args.res, include_hidden=args.all,
                verbose=args.verbose)


if __name__ == "__main__":
    driver()
