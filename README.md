# arf

Go through one or more directories, find all files that match at least a
certain image resolution and/or ratio, and move them to another directory.

Possibly useful if you are disorganized with your images, and want to find e.g.
all the wallpapers or all the photos.

## Requirements

- Python 3
- Pillow (https://python-pillow.org/)
- Checking carefully what you are doing, before doing anything